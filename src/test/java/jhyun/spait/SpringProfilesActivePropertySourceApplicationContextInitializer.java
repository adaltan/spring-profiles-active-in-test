package jhyun.spait;

import java.util.Map;

import jhyun.spait.spring.RootContext;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.PropertySource;

import com.google.common.collect.ImmutableMap;

public class SpringProfilesActivePropertySourceApplicationContextInitializer
		implements
		ApplicationContextInitializer<ConfigurableApplicationContext> {

	@Override
	public void initialize(ConfigurableApplicationContext applicationContext) {
		PropertySource<Map<String, Object>> ps = new MapPropertySource(
				"property-source-spring.profiles.active", ImmutableMap
						.<String, Object> builder()
						.put("spring.profiles.active", RootContext.TEST)
						.build());
		applicationContext.getEnvironment().getPropertySources().addFirst(ps);
	}

}
