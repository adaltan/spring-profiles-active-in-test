package jhyun.spait.tests;

import javax.inject.Inject;
import javax.inject.Named;

import jhyun.spait.MyTestCase;
import junit.framework.Assert;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;

public class ProfilesEnvTestCase extends MyTestCase implements
		MessageSourceAware {

	@Inject
	@Named("greet")
	private String greet;

	@Test
	public void okGreet() {
		Assert.assertNotNull(greet);
		Assert.assertTrue(greet.length() > 0);
		logger.debug(String.format("greet = [%s]", greet));
	}

	@Value("#{curEnv['greet']}")
	private String greet2;

	@Test
	public void okGreet2() {
		Assert.assertNotNull(greet2);
		Assert.assertTrue(greet2.length() > 0);
		logger.debug(String.format("greet2 = [%s]", greet2));
	}

	@Test
	public void okGreet3() {
		final String s = messageSource.getMessage("greet.with.name",
				new String[] { "World" }, null);
		Assert.assertNotNull(s);
		Assert.assertTrue(s.length() > 0);
		logger.debug(String.format("greet3 = [%s]", s));
	}

	private MessageSource messageSource;

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

}
