package jhyun.spait.spring;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertyResolver;

@PropertySource("classpath:/jhyun/spait/spring/profiles/${spring.profiles.active}.properties")
@ComponentScan(basePackages = { "jhyun.spait" })
@Configuration
public class RootContext {

	public static final String TEST = "test";

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Inject
	private Environment curEnv;

	@Bean
	public PropertyResolver curEnv() {
		return curEnv;
	}

	@Bean
	@Named("greet")
	public String greet() {
		return curEnv.getProperty("greet");
	}

	@Bean
	public ResourceBundleMessageSource messageSource() {
		ResourceBundleMessageSource source = new ResourceBundleMessageSource();
		source.setBasename("jhyun/spait/spring/locales/messages");
		source.setUseCodeAsDefaultMessage(true);
		logger.trace("message-source: " + ObjectUtils.toString(source));
		return source;
	}

}
